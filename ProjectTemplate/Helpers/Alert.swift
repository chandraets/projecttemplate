//
//  Alert.swift
//  TryCatch
//
//  Created by Puja Bhagat on 02/12/2021.
//

import Foundation
import UIKit

class Alert {
    class func showBasic(title: String, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //
        }))
        viewController.present(alert, animated: true) {
            //
        }
    }
}
