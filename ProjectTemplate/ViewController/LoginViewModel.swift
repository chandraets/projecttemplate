//
//  LoginViewModel.swift
//  ErrorHandlingDemo
//
//  Created by Puja Bhagat on 02/12/2021.
//

import Foundation

class LoginViewModel: NSObject {
    
    func validateLoginAction(email: String, password: String) throws {
        
        if email.isEmpty {
            throw ErrorHelper.emptyEmail
        }
        
        if password.isEmpty {
            throw ErrorHelper.emptyPassword
        }
        
        if !email.isValidEmail {
            throw ErrorHelper.invalidEmail
        }
        
        if !password.isValidPassword {
            throw ErrorHelper.invalidPassword
        }
    }
}
