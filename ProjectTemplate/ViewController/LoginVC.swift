//
//  LoginVC.swift
//  TryCatch
//
//  Created by Puja Bhagat on 02/12/2021.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    private let viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        do {
            let email = txtEmail.text!
            let password = txtPassword.text!
            try viewModel.validateLoginAction(email: email, password: password)
            //May be you can integrate with API to validate user credential
            //Once username and password valid then may be you can move to next Screen (View Controller)
            
        } catch {
            let loginError = error as? ErrorHelper
            Alert.showBasic(title: loginError!.errorTitle, message: loginError!.errorMessage, viewController: self)
        }
    }
    
    
    
}

