//
//  ErrorHelper.swift
//  ErrorHandlingDemo
//
//  Created by Puja Bhagat on 02/12/2021.
//

import Foundation

enum ErrorHelper: Error {
    case emptyEmail
    case emptyPassword
    case invalidEmail
    case invalidPassword
    
    var errorTitle: String {
        get {
            switch self {
            case .emptyEmail:
                return "Empty Email"
            case .emptyPassword:
                return "Empty Password"
            case .invalidEmail:
                return "Invalid Email"
            case .invalidPassword:
                return "Password Length"
            }
        }
        
    }
    
    var errorMessage: String {
        get {
            switch self {
            case .emptyEmail:
                return "Email should not be empty."
            case .emptyPassword:
                return "Password should not be empty."
            case .invalidPassword:
                return "Passowrd length should be of at least 8 characters."
            case .invalidEmail:
                return "Make sure you have filled with correct email address."
            }
        }
    }
}
